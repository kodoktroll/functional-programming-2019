mymax x y   
    | x > y = x
    | otherwise = y
maxList xs = foldl mymax 0 xs

mymerge [] kanan = kanan
mymerge kiri [] = kiri
mymerge kiri@(x:xs) kanan@(y:ys) 
    | x < y = x : mymerge xs kanan
    | y <= x = y : mymerge kiri ys

mymergeSort xs = mymerge (mymergeSort kiri) (mymergeSort kanan)
    where kiri   = take ((length xs) `quot` 2) xs
          kanan  = drop ((length xs) `quot` 2) xs


myconcat [] = []
myconcat ([]:xs) = myconcat xs
myconcat ((x:xs):ys) = x: myconcat (xs:ys)