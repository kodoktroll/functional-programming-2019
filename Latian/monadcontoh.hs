-- m >>= k = concat (map k m)
pairListComp = [(x,y) | x <- [1,2,3], y <- [4,5]]

pairSugarMonad = do 
    x <- [1,2,3]
    y <- [4,5]
    return (x,y)

pairUglyMonad = ([1,2,3] >>= (\x -> [4,5] >>= (\y -> return (x,y))))