square x = x * x

sumsquare xs = foldr (+) 0 (map square xs)