import Data.List

sumList [] = 0
sumList (x:xs) = x + sumList xs

perms []  = [[]]
perms ls  = [ x:sisa | x<-ls, sisa <- perms (ls \\ [x])]

-- permSplit []  = [[]]
-- permSplit ls  = [ ps ++ [x] ++ qs | rs <- permSplit xs, 
--                                     (ps,qs) <- split rs]
-- Belom ada splitnya.

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs,
                                  y <= x]
                   ++ [x] ++
                   quickSort [y | y <- xs,
                                  y > x]
