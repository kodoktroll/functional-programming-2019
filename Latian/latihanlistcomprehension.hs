testing = [x + y | x <- [1..4], y <- [2..4], x > y]

pembagi x n = n `mod` x == 0

divisor n = [y | y <- [1..n], n `mod` y == 0]

divisor2 n = [y | y <- [1..n], pembagi y n]

triplePythagoras = [(p,q,r) | r <- [5 ..],
                              q <- [r, r-1 .. 1],
                              p <- [q, q-1 .. 1],
                              p*p + q*q == r*r]

-- sieveOfErastothenes: Generate prime number until n
primes = sieve [2 .. ] 
    where 
        sieve (x:xs) = x : sieve [y | y <- xs, y `mod` x /= 0 ]
                            -- y <- [z | z <- xs, z `mod` x /= 0]]

mymap f (x:xs) = f x : (mymap f xs)

-- Soal nomor 2 bagian 1

nomor21 xs = map (+1) xs

-- Soal nomor 2 bagian 2

nomor22 xs ys = concat (map (\x -> map (\y -> (x + y)) ys) xs)

-- Soal nomor 2 bagian 3

nomor23 xs = map (+2) (filter (>3) xs)

-- Soal nomor 2 bagian 4

nomor24 xys = map (\(x,_) -> (x + 3)) xys

-- Soal nomor 2 bagian 5

nomor25 xys = map (\(x,_) -> (x + 3)) (filter (\(x,y) -> (x + y) < 5) xys)

-- Soal nomor 2 bagian 5

-- nomor26 mxs 