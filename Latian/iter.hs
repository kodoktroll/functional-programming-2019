myflip f x y = f y x

iter 0 f x = x
iter n f x = f (iter (n-1) f x)

-- succ2 = \n -> iter n succ