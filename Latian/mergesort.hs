mymerge [] y = y
mymerge x [] = x
mymerge kiri@(x:xs) kanan@(y:ys) 
    | x < y = x : mymerge xs kanan
    | otherwise = y : mymerge kiri ys


halve xs = (take lhx xs, drop lhx xs)
           where lhx = length xs `div` 2

mymergeSort xs 
    | length xs < 2 = xs
    | otherwise = mymerge (mymergeSort kiri) (mymergeSort kanan)
           where (kiri, kanan) = halve xs