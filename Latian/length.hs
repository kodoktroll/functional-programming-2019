-- mylength [] = 0
-- mylength (x:xs) = 1 + length xs

oneify x = 1
mylength xs = sum (map oneify xs)