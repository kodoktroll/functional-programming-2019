primeGenerator = sieve [2 ..] 
    where sieve (x:xs) = x : sieve [y | y <- xs, y `mod` x /= 0 ]

-- zipWith membuat list baru yang isi dari tiap indeksnya adalah hasil
-- operasi antara dua list dengan indeks yang sama

-- tail accepts a list and returns a list without its first item
fiblazy = 1 : 1 : add fiblazy (tail fiblazy)
    where add = zipWith (+)

triplePhyta = [(x,y,z) | z <- [5 ..], y <- [z, z-1 .. 1], x <- [y, y-1 .. 1], x*x + y*y == z*z]