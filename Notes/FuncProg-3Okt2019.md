## List Comprehension
[ x + y | x <- [1..4], y <- [2..4], x > y ]
- Generally, bentukan dari list comprehension adalah sebagai berikut: 
  > [element list | range, predikat (bisa lebih dari satu)]