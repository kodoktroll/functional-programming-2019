# Monad

- Membuat abstraksi jd lebih baik
- Sama seperti design pattern, kita ga terlalu butuh monad buat bikin program.

### Why Monads?

In a purely functional language:
- How do you encode actions with side effects such as IO?
- Is there an elegant way to pass around program state without explicitly threading it in and out every function?
- How do you code up doubly nested for loops?
- What about: Continuation passing style, writing logs, memory transactions...

They're a very general abstraction idea that can be thought of as:
- Containers that wrap values and are composable
- The inverse of pointers
- An abstarction for modelling sequential actions

### Previously

```haskell
data Maybe a = Nothing
             | Just a

lookup :: a -> [(a,b)] -> Maybe b

animalFriends:: [(String, String)]
animalFriends -> [ ("Pony", "Lion")
                , ("Lion", "Manticore")
                , ("Unicorn", "Lepricon") ]
```

- Saat lookup, terdapat nested looping
- Kita abstraksi tipe

```haskell
-- Maybe Monad
-- Memakai bind (>>=) m a -> (a -> m b) -> m b
Just x >>= k = k x
Nothing >>= _ = Nothing

-- return :: a -> m a
return x = Just x


monadicFriendLookup :: [(String, String)] -> Maybe String
monadicFriendLookup animalMap = 
    lookup "Pony" animalMap
    >>= (\ponyFriend -> lookup ponyFriend animalMap
    >>= (\pony2ndFriend -> lookup pony2ndFriend animalMap
    >>= (\friend -> Just friend)))
```

```haskell
-- Makin mantep
sugaryFriendLookup :: [(String, String)] -> Maybe String
sugaryFriendLookup animalMap = do
    ponyFriend <- lookup "Pony" animalMap
    ponyFriend1 <- lookup ponyFriend animalMap
    ponyFriend2 <- lookup ponyFriend1 animalMap
    return ponyFriend2
```

### Threading program state

```haskell
type Sexpr = String

transformStmt :: Sexpr -> Int -> (Sexpr, Int)
transformStmt expr counter = (newExpr, counter+1)
    where newExpr = "(define " ++ var ++ " " ++ expr ++ ")"
          var = "tmpVar" ++ (show counter)
```

```haskell
-- Abstraksi buat trnasformStmt
-- Replace Int -> (Sexpr, Int)
-- Jadi:
newtype State s a = State {
    runState :: s -> (a,s)
}
transformStmt :: Sexpr -> State Int Sexpr
```

```haskell
-- return :: a -> State s a 
return a = State (\s -> (a,s))

-- (>>=) :: State s a -> (a -> State s b) -> State s b
m >>= k = State (\s -> let (a, s') = runState m s 
                       in runState (k a) a')
```

Check Chapter 19